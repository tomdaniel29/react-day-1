import React from 'react';

export default (props) => (
	<div className="home-cocktail" onClick={() => props.onNavigateToCocktail(props.cocktail.idDrink)}>
		<img src={props.cocktail.strDrinkThumb} alt={props.cocktail.strDrink} />
		<div className="home-cocktail-title">
			<span>{props.cocktail.strDrink}</span>
		</div>
	</div>
)
