import React from 'react';
import HomeCocktail from './home-cocktail';

export default (props) => {
	console.log('home render: ', props);
	return (
		<div className="home-cocktails">
			{props.cocktails.map(cocktail => {
				if(cocktail.strDrink.search(props.searchTerm) !== -1) {
					return (
						<HomeCocktail
							key={cocktail.idDrink}
							cocktail={cocktail}
							onNavigateToCocktail={props.onNavigateToCocktail}
						/>
					)
				} else {
					return null;
				}
			})}
		</div>
	)
}
