import React from 'react';
import Search from './search';

export default (props) => (
		<div className="header">
			<div className="header-title">
				<span>Cocktail Party</span>
			</div>
			<Search onSearch={props.onSearch} />
		</div>
)
