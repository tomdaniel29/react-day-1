import React, {Component} from 'react';

export default class extends Component {
	constructor(props) {
		super(props);

		this.inputRef = React.createRef();
		this.handleSearch = this.handleSearch.bind(this);
	}

	handleSearch() {
		this.props.onSearch(this.inputRef.current.value);
	}

	render() {
		return (
			<div className="search-wrapper">
				<input type="text" ref={this.inputRef} />
				<button onClick={this.handleSearch}>Search</button>
			</div>
		)
	}

}
