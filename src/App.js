import React, { Component } from 'react';
import {getCocktails} from './services/cocktails';
import Header from './commons/header';
import Home from './home/home';

class App extends Component {
	constructor(props) {
		console.log('app constructor');
		super(props);

		this.state = {
			cocktails: []
		}

		this.onSearch = this.onSearch.bind(this);
		this.onNavigateToCocktail = this.onNavigateToCocktail.bind(this);
	}

	onNavigateToCocktail(id) {
		this.setState({currentCocktailId: id});
	}

	onSearch(searchTerm) {
		console.log('search: ', searchTerm);
		this.setState({searchTerm: searchTerm});
	}

	componentDidMount() {
		console.log('app didmount');
		getCocktails().then(cocktails => {
			this.setState({cocktails: cocktails});
		})
	}

  render() {
		console.log('app render');
    return (
			<React.Fragment>
				<Header onSearch={this.onSearch} />
				{this.state.currentCocktailId ? (
					<div>single page</div>
				) : (
					<Home
						cocktails={this.state.cocktails}
						searchTerm={this.state.searchTerm}
						onNavigateToCocktail={this.onNavigateToCocktail}
					/>
				)}
			</React.Fragment>
    );
  }
}

export default App;
